/// <reference types="react" />
import * as React$1 from 'react';
import React__default from 'react';
import { DropzoneOptions, DropzoneState } from 'react-dropzone';
import { InputProps, TextFieldProps } from '@material-ui/core';
import { IAllProps } from '@tinymce/tinymce-react';
import * as react_router from 'react-router';
import * as history from 'history';

declare type ErrorType = "noAccess" | "notFound";
interface ErrorLayoutProps {
    body?: React__default.ReactNode;
    errorType?: ErrorType;
    title?: React__default.ReactNode;
}
declare const ErrorLayout: (props: ErrorLayoutProps) => JSX.Element;
//# sourceMappingURL=ErrorLayout.d.ts.map

declare type FlexProps = {
    children?: React__default.ReactNode;
    align?: "center" | "start" | "end";
    className?: string;
    direction?: "row" | "column";
    height?: string;
    justify?: "center" | "start" | "end" | "around" | "between";
    size?: "grow" | "shrink";
};
declare const Flex: React__default.ForwardRefExoticComponent<FlexProps & React__default.RefAttributes<HTMLDivElement>>;
//# sourceMappingURL=FlexLayout.d.ts.map

declare type GridProps = {
    children?: React__default.ReactNode;
    className?: string;
    cols?: number;
    responsive?: boolean;
    rows?: number;
    template?: Array<number>;
};
declare const Grid: React__default.ForwardRefExoticComponent<GridProps & React__default.RefAttributes<HTMLDivElement>>;
//# sourceMappingURL=GridLayout.d.ts.map

interface LoaderProps {
    /**
     * css class name
     */
    className?: string;
    horizontal?: boolean;
    light?: boolean;
    title?: string;
}
declare const Loader: (props: LoaderProps) => JSX.Element;

declare type Status = "fail" | "loading" | "ready" | "saving" | "success";
declare type LoadingLayoutProps = {
    children?: React__default.ReactNode;
    error?: React__default.ReactElement;
    errorText?: string;
    keepChildrenMounted?: boolean;
    loading?: React__default.ReactElement;
    loadingText?: string;
    saving?: React__default.ReactElement;
    savingText?: string;
    status?: Status;
};
declare const LoadingLayout: {
    (props: LoadingLayoutProps): JSX.Element;
    defaultProps: {
        status: string;
    };
};

declare type UploadFileRowProps = Pick<FileUploadProps, "comment"> & {
    /**
     * file of upload file
     */
    fileName: string;
    /**
     * enable/disable edit file name
     */
    editable?: boolean;
    /**
     * file name edit input placeholder
     */
    placeholder?: any;
    /**
     * file size
     */
    size: number;
    /**
     * file type
     */
    type: string;
    /**
     * flag to show/hide toggle comment button
     */
    commentable?: boolean;
    /**
     * change file name event
     * @param name: file name
     * @param valid: check file name validity, empty is invalid
     */
    onEditFileName: (name: string, valid: boolean) => void;
    /**
     * remove upload file on list
     */
    onDelete: () => void;
    /**
     * add comment to file event
     * @param comment comment text
     */
    onEditComment?(e: React__default.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, comment: string): void;
};
declare const _default$2: React__default.MemoExoticComponent<({ editable, placeholder, fileName, size, comment, commentable, type, onEditFileName, onDelete, onEditComment, }: UploadFileRowProps) => JSX.Element | null>;

declare type FileUploadProps = {
    file: File;
    comment?: string;
};
declare type FileChangeEvent = "add" | "remove" | "changeName" | "editComment";
interface UploadChangeEvent {
    data: any;
    event?: FileChangeEvent;
    files: FileUploadProps[];
    valid?: boolean;
}
declare type UploadProps = DropzoneOptions & {
    children?(props: {
        dropzone: DropzoneState;
        UploadDisplay: React__default.ReactElement;
    }): React__default.ReactElement;
    className?: string;
    editFile?: boolean;
    files?: FileUploadProps[];
    fullHeight?: boolean;
    onUploadChange?(event: UploadChangeEvent): void;
    Component?: {
        UploadFileRow?: React__default.ComponentType<UploadFileRowProps>;
    };
    ComponentProps?: {
        UploadFileRowProps?: Record<string, any>;
    };
    required?: boolean;
};
declare const _default$1: React__default.MemoExoticComponent<{
    ({ onUploadChange, editFile, files, required, fullHeight, className, children, multiple, Component, ComponentProps, ...props }: UploadProps): JSX.Element | null;
    defaultProps: {
        multiple: boolean;
        ComponentProps: {};
        Component: {};
        files: never[];
    };
}>;

declare type UploadRowContextProps = {
    cmtVisible?: boolean;
    isReadOnly?: boolean;
    hasError?: boolean;
    _setContext?: React.Dispatch<React.SetStateAction<UploadRowContextProps>>;
};
declare const UploadRowContext: React$1.Context<UploadRowContextProps>;

declare type UploadRowProps = Pick<UploadRowContextProps, "hasError" | "isReadOnly"> & {
    children?: React__default.ReactNode;
};
declare const UploadRow: ({ children, isReadOnly, hasError }: UploadRowProps) => JSX.Element;
//# sourceMappingURL=UploadRow.d.ts.map

declare type UploadRowActionsProps = {
    /**
     *
     */
    relativeUrl?: string;
    /**
     * file data
     */
    fileData?: any;
    /**
     * flag to show/hide toggle comment button
     */
    commentable?: boolean;
    /**
     * remove file on list to upload
     */
    onDelete?(): void;
};
declare const UploadRowActions: {
    ({ fileData, commentable, relativeUrl, onDelete }: UploadRowActionsProps): JSX.Element;
    defaultProps: {
        fileData: {};
    };
};
//# sourceMappingURL=UploadRowActions.d.ts.map

declare type UploadRowCommentProps = Pick<InputProps, "onChange"> & {
    /**
     * default comment value
     */
    comment?: string;
};
declare const _default: React__default.MemoExoticComponent<{
    ({ comment, onChange }: UploadRowCommentProps): JSX.Element;
    defaultProps: {
        comment: string;
    };
}>;
//# sourceMappingURL=UploadRowComment.d.ts.map

declare type UploadRowFileNameProps = Pick<TextFieldProps, "placeholder"> & {
    children?: React__default.ReactNode;
    isReadOnly?: boolean;
    fileName?: string;
    onEditName?(name: string, ext: string): void;
};
declare const UploadRowFileName: {
    ({ isReadOnly, fileName, placeholder, children, onEditName }: UploadRowFileNameProps): JSX.Element;
    defaultProps: {
        fileName: string;
    };
};

declare type UploadRowHelperProps<E = {
    id?: string | null;
    error?: string | null;
    translate?: boolean | null;
}> = {
    errors?: E[];
    children?: React__default.ReactNode;
};
declare const UploadRowHelper: ({ children, errors }: UploadRowHelperProps) => JSX.Element;

declare type PickerProviderProps = {
    children?: React__default.ReactNode;
    locale?: any;
};
declare const PickerProvider: ({ children, locale }: PickerProviderProps) => JSX.Element;
//# sourceMappingURL=PickerProvider.d.ts.map

declare type TinyEditorProps = IAllProps;
declare const TinyEditor: ({ ...props }: TinyEditorProps) => JSX.Element | null;
//# sourceMappingURL=TinyEditor.d.ts.map

declare const isUndefined: (value: any) => boolean;
declare const isNull: (value: any) => boolean;
declare const isNil: (value: any) => boolean;
declare const isArray: (value: any) => boolean;
declare const isEmptyArray: (value: any) => boolean;
declare const isObject: (value: any) => boolean;
declare const isEmptyObject: (value: any) => boolean;
declare const isObjectLiked: (value: any) => boolean;
declare const isEmpty: (value: any) => boolean;
declare const isDate: (value: any) => boolean;
declare const isSymbol: (value: any) => boolean;
declare const isError: (value: any) => boolean;
declare const isRegExp: (value: any) => boolean;
declare const isFunction: (value: any) => boolean;
declare const isFolderData: (data: any) => any;
declare const getEnumList: <T extends {}>(en: T) => [string, unknown][];
interface WebEditorSupport {
    supported: boolean;
    editorSlug?: "_spreadsheet" | "_document" | "_pdf";
}
declare const checkEditorSupportsFile: (fileName: string) => WebEditorSupport;

declare function addSearchParam(search: URLSearchParams | string, key: string, value: any): URLSearchParams;
declare const debounce: (handler: any, delay?: number, now?: boolean) => (this: any) => void;
declare const GetReadableFileSize: (fileSizeInBytes: number) => string;

declare function useRouter(): {
    searchParams: URLSearchParams;
    action: history.Action;
    push: {
        (path: string, state?: unknown): void;
        (location: history.LocationDescriptor<unknown>): void;
    };
    replace: {
        (path: string, state?: unknown): void;
        (location: history.LocationDescriptor<unknown>): void;
    };
    pathname: string;
    state: Record<string, string>;
    query: any;
    match: react_router.match<{}>;
    location: history.Location<unknown>;
    history: history.History<unknown>;
};

export { ErrorLayout, Flex as FlexLayout, GetReadableFileSize, Grid as GridLayout, Loader, LoadingLayout, PickerProvider, Status, TinyEditor, _default$1 as Upload, _default$2 as UploadFileRow, UploadRow, UploadRowActions, _default as UploadRowComment, UploadRowContext, UploadRowFileName, UploadRowHelper, addSearchParam, checkEditorSupportsFile, debounce, getEnumList, isArray, isDate, isEmpty, isEmptyArray, isEmptyObject, isError, isFolderData, isFunction, isNil, isNull, isObject, isObjectLiked, isRegExp, isSymbol, isUndefined, useRouter };
