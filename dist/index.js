'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var React = require('react');
var core = require('@material-ui/core');
var fa = require('react-icons/fa');
var reactRouterDom = require('react-router-dom');
var reactI18next = require('react-i18next');
var clsx = require('clsx');
var reactDropzone = require('react-dropzone');
var reactIs = require('react-is');
var isEqual = require('fast-deep-equal/react');
var pickers = require('@material-ui/pickers');
var moment = require('moment');
var MomentUtils = require('@date-io/moment');
var tinymceReact = require('@tinymce/tinymce-react');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var React__default = /*#__PURE__*/_interopDefaultLegacy(React);
var clsx__default = /*#__PURE__*/_interopDefaultLegacy(clsx);
var isEqual__default = /*#__PURE__*/_interopDefaultLegacy(isEqual);
var moment__default = /*#__PURE__*/_interopDefaultLegacy(moment);
var MomentUtils__default = /*#__PURE__*/_interopDefaultLegacy(MomentUtils);

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spreadArray(to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || from);
}

var Flex = React__default['default'].forwardRef(function (props, ref) {
    var componentProps = __assign({ className: clsx__default['default']("flex", props.align && "f-align-" + props.align, props.direction && "f-direction-" + props.direction, props.justify && "f-justify-" + props.justify, props.size && "f-size-" + props.size, props.className) }, (props.height && {
        style: {
            height: props.height,
        },
    }));
    return (React__default['default'].createElement("div", __assign({}, componentProps, { ref: ref }), props.children));
});

var Grid = React__default['default'].forwardRef(function (props, ref) {
    var componentProps = __assign({ className: clsx__default['default']("grid", props.rows && "g-rows-" + props.rows, props.cols && "g-cols-" + props.cols, props.responsive && "g-responsive", props.className) }, (props.template && {
        style: {
            gridTemplateColumns: props.template.join("fr ") + "fr",
        },
    }));
    return (React__default['default'].createElement("div", __assign({}, componentProps, { ref: ref }), props.children));
});

var errorIcon = function (errorType) {
    switch (errorType) {
        case "noAccess":
            return React__default['default'].createElement(fa.FaMinusCircle, { size: 32 });
        case "notFound":
            return React__default['default'].createElement(fa.FaQuestionCircle, { size: 32 });
        default:
            return React__default['default'].createElement(fa.FaExclamationCircle, { size: 32 });
    }
};
var ErrorLayout = function (props) {
    var history = reactRouterDom.useHistory();
    var t = reactI18next.useTranslation("common").t;
    return (React__default['default'].createElement(Flex, { align: "center", justify: "center", height: "100%", size: "grow", className: "pad" },
        React__default['default'].createElement(Grid, { className: "text-center" },
            React__default['default'].createElement("div", null, errorIcon(props.errorType)),
            React__default['default'].createElement("div", null, props.body),
            React__default['default'].createElement("div", null,
                React__default['default'].createElement(core.Button, { onClick: function () { return history.goBack(); } }, t("compound.goBack"))))));
};

var Loader = function (props) {
    return (React__default['default'].createElement("div", { className: clsx__default['default']("loader-container", props.horizontal && "loader-horizontal", props.className) },
        React__default['default'].createElement("div", { className: clsx__default['default']("loader", props.light && "loader-light") }),
        props.title && React__default['default'].createElement("div", { className: "loader-title" }, props.title)));
};

var LoadingLayout = function (props) {
    var _a;
    if (props.keepChildrenMounted)
        return (React__default['default'].createElement(React__default['default'].Fragment, null,
            ["loading", "saving"].includes((_a = props.status) !== null && _a !== void 0 ? _a : "") && (React__default['default'].createElement("div", { className: "loading-overlay" },
                React__default['default'].createElement(LoadingLayoutRender, __assign({}, props)))),
            props.children));
    return React__default['default'].createElement(LoadingLayoutRender, __assign({}, props));
};
LoadingLayout.defaultProps = {
    status: "ready",
};
var LoadingLayoutRender = function (props) {
    var _a, _b, _c, _d, _e, _f;
    var t = reactI18next.useTranslation("common").t;
    switch (props.status) {
        case "loading":
            return (_a = props.loading) !== null && _a !== void 0 ? _a : React__default['default'].createElement(Loader, { title: (_b = props.loadingText) !== null && _b !== void 0 ? _b : t("labels.loading") });
        case "saving":
            return (_c = props.saving) !== null && _c !== void 0 ? _c : React__default['default'].createElement(Loader, { title: (_d = props.savingText) !== null && _d !== void 0 ? _d : t("labels.saving") });
        case "fail":
            return (_e = props.error) !== null && _e !== void 0 ? _e : React__default['default'].createElement(ErrorLayout, { body: (_f = props.errorText) !== null && _f !== void 0 ? _f : t("labels.error") });
        default:
            if (!props.keepChildrenMounted)
                return React__default['default'].createElement(React__default['default'].Fragment, null, props.children);
            return null;
    }
};

var uploadRowContextDefault = {
    cmtVisible: false,
    isReadOnly: false,
};
Object.freeze(uploadRowContextDefault);
var UploadRowContext = React.createContext(uploadRowContextDefault);
var useUploadRowContext = function () {
    var _a = React.useContext(UploadRowContext), _setContext = _a._setContext, context = __rest(_a, ["_setContext"]);
    var setCmtVisible = function (cmtVisible) {
        _setContext === null || _setContext === void 0 ? void 0 : _setContext(function (prev) { return (__assign(__assign({}, prev), { cmtVisible: cmtVisible })); });
    };
    return __assign(__assign({}, context), { setCmtVisible: setCmtVisible });
};

function styleInject(css, ref) {
  if ( ref === void 0 ) ref = {};
  var insertAt = ref.insertAt;

  if (!css || typeof document === 'undefined') { return; }

  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';

  if (insertAt === 'top') {
    if (head.firstChild) {
      head.insertBefore(style, head.firstChild);
    } else {
      head.appendChild(style);
    }
  } else {
    head.appendChild(style);
  }

  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
}

var css_248z = ".upload-row {\n  display: flex;\n  flex-wrap: wrap;\n  gap: 0.35rem; }\n";
styleInject(css_248z);

var UploadRow = function (_a) {
    var children = _a.children, isReadOnly = _a.isReadOnly, hasError = _a.hasError;
    var _b = __read(React.useState(uploadRowContextDefault), 2), context = _b[0], _setContext = _b[1];
    return (React__default['default'].createElement("div", { className: clsx__default['default']("upload-row", {
            "upload-row-comment-visible": context.cmtVisible,
            "upload-row-error": hasError,
        }) },
        React__default['default'].createElement(UploadRowContext.Provider, { value: __assign(__assign({}, context), { isReadOnly: isReadOnly, hasError: hasError, _setContext: _setContext }) }, children)));
};

function addSearchParam(search, key, value) {
    var result = new URLSearchParams(search);
    if (result.has(key)) {
        result.set(key, value);
    }
    else {
        result.append(key, value);
    }
    return result;
}
var debounce = function (handler, delay, now) {
    if (delay === void 0) { delay = 0; }
    if (now === void 0) { now = false; }
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!now)
                handler.apply(context, args);
        };
        var callNow = now && !timeout;
        if (timeout !== null)
            clearTimeout(timeout);
        timeout = setTimeout(later, delay);
        if (callNow)
            handler.apply(context, args);
    };
};
var GetReadableFileSize = function (fileSizeInBytes) {
    var i = -1;
    var byteUnits = [" kB", " MB", " GB", " TB", "PB", "EB", "ZB", "YB"];
    do {
        fileSizeInBytes = fileSizeInBytes / 1024;
        i++;
    } while (fileSizeInBytes > 1024);
    return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
};

function useRouter() {
    var params = reactRouterDom.useParams();
    var location = reactRouterDom.useLocation();
    var history = reactRouterDom.useHistory();
    var match = reactRouterDom.useRouteMatch();
    var searchParams = new URLSearchParams(location.search);
    // Return our custom router object
    // Memoize so that a new object is only returned if something changes
    return React.useMemo(function () {
        return {
            searchParams: searchParams,
            action: history.action,
            // For convenience add push(), replace(), pathname at top level
            push: history.push,
            replace: history.replace,
            pathname: location.pathname,
            state: location.state,
            // Merge params and parsed query string into single "query" object
            // so that they can be used interchangeably.
            // Example: /:topic?sort=popular -> { topic: "react", sort: "popular" }
            query: __assign(__assign({}, _iterateQueries(searchParams)), params),
            // Include match, location, history objects so we have
            // access to extra React Router functionality if needed.
            match: match,
            location: location,
            history: history,
        };
    }, [params, match, location, history]);
}
function _iterateQueries(params) {
    return Object.entries(params).reduce(function (acc, _a) {
        var _b;
        var _c = __read(_a, 2), key = _c[0], value = _c[1];
        return (__assign(__assign({}, acc), (_b = {}, _b[key] = value, _b)));
    }, {});
}

var UploadRowActions = function (_a) {
    var fileData = _a.fileData, commentable = _a.commentable, relativeUrl = _a.relativeUrl, onDelete = _a.onDelete;
    var searchParams = useRouter().searchParams;
    var _b = useUploadRowContext(), setCmtVisible = _b.setCmtVisible, cmtVisible = _b.cmtVisible, isReadOnly = _b.isReadOnly;
    var btnProps = isReadOnly
        ? {
            target: "_blank",
            component: reactRouterDom.Link,
            to: {
                search: addSearchParam(searchParams, "id", fileData.parentId).toString(),
                pathname: (relativeUrl !== null && relativeUrl !== void 0 ? relativeUrl : "") + "/_forms/edit",
            },
        }
        : {
            onClick: onDelete,
        };
    return (React__default['default'].createElement("div", { className: "upload-row-actions" },
        React__default['default'].createElement(core.IconButton, __assign({ tabIndex: -1, disableRipple: true, title: isReadOnly ? "open edit form" : "remove file" }, btnProps), isReadOnly ? React__default['default'].createElement(fa.FaEdit, null) : React__default['default'].createElement(fa.FaTimes, null)),
        commentable && !isReadOnly && (React__default['default'].createElement(core.IconButton, { tabIndex: -1, onClick: function () { return setCmtVisible(!cmtVisible); }, title: "file comment" },
            React__default['default'].createElement(fa.FaComment, null)))));
};
UploadRowActions.defaultProps = {
    fileData: {},
};

var UploadRowComment = function (_a) {
    var comment = _a.comment, onChange = _a.onChange;
    var t = reactI18next.useTranslation().t;
    var cmtVisible = useUploadRowContext().cmtVisible;
    var _b = __read(React.useState(comment || ""), 2), words = _b[0], setWord = _b[1];
    var debounceRef = React.useRef(debounce(function () {
        onChange === null || onChange === void 0 ? void 0 : onChange(this);
    }, 500));
    var _onChange = function (e) {
        e.persist();
        if (e.target.value.length <= 1000) {
            setWord(e.target.value);
            var dispatchOnChange = debounceRef.current.bind(e);
            dispatchOnChange();
        }
    };
    return (React__default['default'].createElement(core.Collapse, { in: cmtVisible },
        React__default['default'].createElement("div", { className: "upload-row-comment" },
            React__default['default'].createElement(core.Input, { placeholder: t("labels.comments"), multiline: true, fullWidth: true, inputProps: {
                    tabIndex: cmtVisible ? 0 : -1,
                }, value: words, onChange: _onChange }),
            React__default['default'].createElement("small", { className: "upload-row-comment-wordcount" }, "" + words.length,
                "/1000"))));
};
UploadRowComment.defaultProps = {
    comment: "",
};
var UploadRowComment$1 = React.memo(UploadRowComment, function (prev, next) { return true; });

var UploadRowFileName = function (_a) {
    var isReadOnly = _a.isReadOnly, fileName = _a.fileName, placeholder = _a.placeholder, children = _a.children, onEditName = _a.onEditName;
    var n = fileName || "";
    var dot = n.lastIndexOf(".");
    var ext = n.slice(dot, n.length);
    var _b = __read(React.useState(n.slice(0, dot)), 2), name = _b[0], setName = _b[1];
    var _onChange = React.useCallback(function (e) {
        e.persist();
        var nm = e.target.value;
        setName(nm);
        if (nm !== n.slice(0, dot))
            onEditName === null || onEditName === void 0 ? void 0 : onEditName(nm, ext);
    }, [n, onEditName, ext]);
    return isReadOnly ? (React__default['default'].createElement("div", { className: "upload-row-filename" }, children)) : (React__default['default'].createElement(core.TextField, { error: !name.length, value: name, placeholder: placeholder, onChange: _onChange }));
};
UploadRowFileName.defaultProps = {
    fileName: "",
};

var UploadRowHelper = function (_a) {
    var children = _a.children, errors = _a.errors;
    var t = reactI18next.useTranslation("common").t;
    return (React__default['default'].createElement("div", { className: "upload-row-helper" }, errors ? ([errors]
        .flat()
        .filter(Boolean)
        .map(function (error, i) { return React__default['default'].createElement("span", { key: i }, error.translate ? t("error." + error.error) : error.error); })) : (React__default['default'].createElement("span", null, children))));
};

var UploadFileRow = function (_a) {
    var editable = _a.editable, placeholder = _a.placeholder, fileName = _a.fileName, size = _a.size, comment = _a.comment, commentable = _a.commentable; _a.type; var onEditFileName = _a.onEditFileName, onDelete = _a.onDelete, onEditComment = _a.onEditComment;
    var uploadContext = React.useContext(UploadContext);
    if (process.env.mode === "development") {
        if (!uploadContext) {
            console.error("Please provide upload context to get Upload working properly");
            return null;
        }
    }
    var files = uploadContext.files;
    var fileData = React.useMemo(function () { return (files ? files[fileName] : {}); }, [files, fileName]);
    var uploaded = !!fileData;
    var error = uploaded && !!fileData.errors;
    var isReadOnly = !((editable && !uploaded) || error);
    return (React__default['default'].createElement(UploadRow, { isReadOnly: isReadOnly },
        React__default['default'].createElement(Flex, { size: "grow", direction: "column" },
            React__default['default'].createElement(UploadRowFileName, { fileName: fileName, isReadOnly: isReadOnly, placeholder: placeholder, onEditName: function (name, ext) {
                    if (onEditFileName)
                        onEditFileName("" + name + ext, !!name);
                } }, fileName + " - " + GetReadableFileSize(size)),
            commentable && (React__default['default'].createElement(UploadRowComment$1, { comment: comment, onChange: function (e) { return onEditComment === null || onEditComment === void 0 ? void 0 : onEditComment(e, e.target.value); } })),
            React__default['default'].createElement(UploadRowHelper, null, !isReadOnly && React__default['default'].createElement("small", null, fileName + " - " + GetReadableFileSize(size)))),
        React__default['default'].createElement(UploadRowActions, { fileData: fileData, commentable: commentable, onDelete: onDelete })));
};
var FileRow = React.memo(UploadFileRow);

var isUndefined = function (value) {
    return value === undefined;
};
var isNull = function (value) {
    return value === null;
};
var isNil = function (value) {
    return isNull(value) || isUndefined(value);
};
var isArray = function (value) {
    return Array.isArray(value);
};
var isEmptyArray = function (value) {
    return isArray(value) ? !value.length : true;
};
var isObject = function (value) {
    return !!(value && typeof value === "object" && value.constructor === Object);
};
var isEmptyObject = function (value) {
    return isObject(value) ? !Object.keys(value).length : true;
};
var isObjectLiked = function (value) {
    return isArray(value) || isObject(value);
};
var isEmpty = function (value) {
    return isNil(value) || value === "" || isEmptyObject(value) || isEmptyArray(value);
};
var isDate = function (value) {
    return value instanceof Date;
};
var isSymbol = function (value) {
    return typeof value === "symbol";
};
var isError = function (value) {
    return value instanceof Error && typeof value.message !== "undefined";
};
var isRegExp = function (value) {
    return !!(value && typeof value === "object" && value.constructor === RegExp);
};
var isFunction = function (value) {
    return typeof value === "function";
};
var isFolderData = function (data) {
    return isObject(data) && data.hasOwnProperty("ParentFolderId");
};
var getEnumList = function (en) {
    return Object.entries(en).filter(function (_a) {
        var _b = __read(_a, 2); _b[0]; var value = _b[1];
        return typeof value === "number";
    });
};
var checkEditorSupportsFile = function (fileName) {
    var _a;
    if (fileName != null && fileName.length > 1) {
        var re = new RegExp(/(?:\.([^.]+))?$/);
        var ext = (_a = re.exec(fileName)) === null || _a === void 0 ? void 0 : _a[0];
        switch (ext === null || ext === void 0 ? void 0 : ext.toLowerCase()) {
            case ".dotx":
            case ".docx":
            case ".docm":
            case ".dotm":
            case ".dot":
            case ".doc":
            case ".rtf":
            case ".txt":
            case ".xml":
                return {
                    supported: true,
                    editorSlug: "_document",
                };
            case ".xls":
            case ".xlsx":
            case ".csv":
                return {
                    supported: true,
                    editorSlug: "_spreadsheet",
                };
            case ".pdf":
                return {
                    supported: true,
                    editorSlug: "_pdf",
                };
            default:
                return {
                    supported: false,
                };
        }
    }
    return {
        supported: false,
    };
};

var UploadContext = React.createContext({
    progress: 0,
    speed: 0,
    status: "ready",
    files: {},
    reset: function () { },
});
var Upload = function (_a) {
    var onUploadChange = _a.onUploadChange, editFile = _a.editFile, files = _a.files; _a.required; var fullHeight = _a.fullHeight, className = _a.className; _a.children; var multiple = _a.multiple, Component = _a.Component, ComponentProps = _a.ComponentProps, props = __rest(_a, ["onUploadChange", "editFile", "files", "required", "fullHeight", "className", "children", "multiple", "Component", "ComponentProps"]);
    var uploadContext = React.useContext(UploadContext);
    if (process.env.mode === "development") {
        if (!uploadContext) {
            console.error("Please provide upload context to get Upload working properly");
            return null;
        }
    }
    var progress = uploadContext.progress, speed = uploadContext.speed, status = uploadContext.status, reset = uploadContext.reset;
    var t = reactI18next.useTranslation("common").t;
    var prevFiles = React.useRef(files !== null && files !== void 0 ? files : []);
    var validRef = React.useRef({});
    var UploadFileRow = (Component || { UploadFileRow: null }).UploadFileRow;
    var UploadFileRowProps = (ComponentProps || { UploadFileRowProps: null }).UploadFileRowProps;
    var _b = __read(React.useState(prevFiles.current.map(function (f) { return ({
        name: f.file.name,
        size: f.file.size,
        type: f.file.type,
        comment: f.comment,
    }); })), 2), fileData = _b[0], setFileData = _b[1];
    var _updateFileData = React.useCallback(function (fs) {
        if (!files) {
            setFileData(function (_) {
                return fs.map(function (f) { return ({
                    name: f.file.name,
                    size: f.file.size,
                    type: f.file.type,
                    comment: f.comment,
                }); });
            });
        }
    }, [files]);
    var _onUploadChange = React.useCallback(function (event, data) {
        onUploadChange === null || onUploadChange === void 0 ? void 0 : onUploadChange({
            event: event,
            data: data,
            files: prevFiles.current,
            valid: isEmptyObject(validRef.current),
        });
    }, [onUploadChange]);
    var dropzone = __rest(reactDropzone.useDropzone(__assign(__assign({ accept: props.accept, noKeyboard: true }, props), { multiple: multiple, disabled: props.disabled, onDrop: function (files) {
            prevFiles.current = files.reduce(function (arr, file) { return __spreadArray(__spreadArray([], __read(arr)), [{ file: file }]); }, multiple ? prevFiles.current : []);
            _updateFileData(prevFiles.current);
            _onUploadChange("add", files);
        } })), []);
    var _changeFile = React.useCallback(function (index) { return function (name, valid) {
        validRef.current[index] = valid;
        if (valid)
            delete validRef.current[index];
        else
            validRef.current[index] = false;
        var f = prevFiles.current[index].file;
        prevFiles.current[index].file = new File([f], name, {
            type: f.type,
        });
        _onUploadChange("changeName", prevFiles.current[index]);
    }; }, [_onUploadChange]);
    var _removeFile = React.useCallback(function (index) { return function () {
        delete validRef.current[index];
        prevFiles.current = prevFiles.current.filter(function (_, i) { return i !== index; });
        _updateFileData(prevFiles.current);
        _onUploadChange("remove", index);
    }; }, [_onUploadChange]);
    var _onEditComment = React.useCallback(function (index) { return function (_, comment) {
        prevFiles.current[index].comment = comment;
        _updateFileData(prevFiles.current);
        _onUploadChange("editComment", comment);
    }; }, [_onUploadChange]);
    React.useEffect(function () {
        if (files) {
            // set progress to 0 for repeat uploads
            reset();
            var fd_1 = files.map(function (f) { return ({
                name: f.file.name,
                size: f.file.size,
                type: f.file.type,
                comment: f.comment,
            }); });
            if (!isEqual__default['default'](fd_1, fileData)) {
                setFileData(function (_) { return fd_1; });
                prevFiles.current = files;
            }
        }
    }, [files]);
    return (React__default['default'].createElement("div", { className: clsx__default['default']("upload-container", {
            fullHeight: fullHeight,
            hasFile: prevFiles.current.length,
        }, className) },
        React__default['default'].createElement(LoadingLayout, { error: React__default['default'].createElement("div", { className: "error" },
                React__default['default'].createElement("div", null, t("upload.fail")),
                React__default['default'].createElement(core.Button, { onClick: function () {
                        reset();
                        //setStatus('ready');
                    } }, t("upload.retry"))), status: status, loading: React__default['default'].createElement("div", { className: "upload-status" },
                React__default['default'].createElement(core.LinearProgress, { variant: "determinate", value: progress }),
                React__default['default'].createElement("div", { className: "upload-status-stats" },
                    React__default['default'].createElement("div", { className: "upload-status-progress" }, progress.toFixed(1) + " %"),
                    React__default['default'].createElement("div", { className: "upload-status-speed" }, speed.toFixed(1) + " MB/s"))) },
            React__default['default'].createElement("div", __assign({}, dropzone.getRootProps({
                className: "dropzone",
            })),
                React__default['default'].createElement("input", __assign({}, dropzone.getInputProps())),
                t("compound.dragToUpload"))),
        !!fileData.length && (React__default['default'].createElement("div", { className: "filerows" }, fileData.map(function (file, index) {
            return React.createElement(reactIs.isValidElementType(UploadFileRow) ? UploadFileRow : FileRow, __assign({ comment: file.comment, editable: editFile, fileName: file.name, key: "" + file.name + index, onEditFileName: _changeFile(index), onDelete: _removeFile(index), onEditComment: _onEditComment(index), placeholder: t("upload.fileNamePlaceholder"), size: file.size, type: file.type }, UploadFileRowProps), null);
        })))));
};
Upload.defaultProps = {
    multiple: false,
    ComponentProps: {},
    Component: {},
    files: [],
};
var Upload$1 = React.memo(Upload);

// using this instead of the original from @material-ui/pickers
var PickerProvider = function (_a) {
    var children = _a.children, locale = _a.locale;
    return (React__default['default'].createElement(pickers.MuiPickersUtilsProvider, { libInstance: moment__default['default'], utils: MomentUtils__default['default'], locale: locale }, children));
};

var TinyEditor = function (_a) {
    var props = __rest(_a, []);
    var editorRef = React.useRef(null);
    var i18n = reactI18next.useTranslation("common").i18n;
    var _b = __read(React.useState(false), 2), reload = _b[0], setReload = _b[1];
    var language = React.useMemo(function () {
        switch (i18n.language) {
            case "fr":
                return "fr_FR";
            case "es":
                return "es";
            default:
                return undefined;
        }
    }, [i18n.language]);
    var init = React.useMemo(function () { return (__assign({ language: language }, props.init)); }, [props.init, language]);
    React.useEffect(function () {
        setTimeout(function () {
            setReload(false);
        }, 100);
        return function () {
            // not in use atm but dont remove
            // if (editorRef.current) {
            //     console.log("language", (editorRef.current.elementRef.current as HTMLDivElement).clientHeight);
            // }
            setReload(true);
        };
    }, [language]);
    React.useEffect(function () {
        var _a;
        if (((_a = editorRef.current) === null || _a === void 0 ? void 0 : _a.inline) && init.height) {
            var ele = document.getElementById(editorRef.current.id);
            ele.style.minHeight = init.height + "px";
        }
    }, []);
    if (reload)
        return null;
    return (React__default['default'].createElement(tinymceReact.Editor, __assign({}, props, { onKeyDown: function (evt, editor) {
            if (evt.key === "Tab") {
                evt.preventDefault();
                evt.stopPropagation();
                // tab pressed
                editor.insertContent("&#9;"); // inserts tab
            }
        }, ref: editorRef, init: init, apiKey: "4m0kfbftg4xkhw6ymqunzr8uroevxmvqz8tha79777bvg19q" })));
};

exports.ErrorLayout = ErrorLayout;
exports.FlexLayout = Flex;
exports.GetReadableFileSize = GetReadableFileSize;
exports.GridLayout = Grid;
exports.Loader = Loader;
exports.LoadingLayout = LoadingLayout;
exports.PickerProvider = PickerProvider;
exports.TinyEditor = TinyEditor;
exports.Upload = Upload$1;
exports.UploadFileRow = FileRow;
exports.UploadRow = UploadRow;
exports.UploadRowActions = UploadRowActions;
exports.UploadRowComment = UploadRowComment$1;
exports.UploadRowContext = UploadRowContext;
exports.UploadRowFileName = UploadRowFileName;
exports.UploadRowHelper = UploadRowHelper;
exports.addSearchParam = addSearchParam;
exports.checkEditorSupportsFile = checkEditorSupportsFile;
exports.debounce = debounce;
exports.getEnumList = getEnumList;
exports.isArray = isArray;
exports.isDate = isDate;
exports.isEmpty = isEmpty;
exports.isEmptyArray = isEmptyArray;
exports.isEmptyObject = isEmptyObject;
exports.isError = isError;
exports.isFolderData = isFolderData;
exports.isFunction = isFunction;
exports.isNil = isNil;
exports.isNull = isNull;
exports.isObject = isObject;
exports.isObjectLiked = isObjectLiked;
exports.isRegExp = isRegExp;
exports.isSymbol = isSymbol;
exports.isUndefined = isUndefined;
exports.useRouter = useRouter;
//# sourceMappingURL=index.js.map
