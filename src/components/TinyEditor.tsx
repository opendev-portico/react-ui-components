import React, { useEffect, useMemo, useRef, useState } from "react";
import { Editor, IAllProps } from "@tinymce/tinymce-react";
import { useTranslation } from "react-i18next";

type TinyEditorProps = IAllProps;

const TinyEditor = ({ ...props }: TinyEditorProps) => {
    const editorRef = useRef<any>(null);
    const { i18n } = useTranslation("common");
    const [reload, setReload] = useState(false);

    const language = useMemo(() => {
        switch (i18n.language) {
            case "fr":
                return "fr_FR";
            case "es":
                return "es";
            default:
                return undefined;
        }
    }, [i18n.language]);

    const init = useMemo(
        () => ({
            language,
            ...props.init,
        }),
        [props.init, language]
    );

    useEffect(() => {
        setTimeout(() => {
            setReload(false);
        }, 100);
        return () => {
            // not in use atm but dont remove
            // if (editorRef.current) {
            //     console.log("language", (editorRef.current.elementRef.current as HTMLDivElement).clientHeight);
            // }
            setReload(true);
        };
    }, [language]);

    useEffect(() => {
        if (editorRef.current?.inline && init.height) {
            const ele = document.getElementById(editorRef.current.id) as HTMLElement;
            ele.style.minHeight = init.height + "px";
        }
    }, []);

    if (reload) return null;
    return (
        <Editor
            {...props}
            onKeyDown={(evt, editor) => {
                if (evt.key === "Tab") {
                    evt.preventDefault();
                    evt.stopPropagation();
                    // tab pressed
                    editor.insertContent("&#9;"); // inserts tab
                }
            }}
            ref={editorRef}
            init={init}
            apiKey="4m0kfbftg4xkhw6ymqunzr8uroevxmvqz8tha79777bvg19q"
        />
    );
};

export default TinyEditor;
