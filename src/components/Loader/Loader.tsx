import React from "react";
import clsx from "clsx";

import "./loader.scss";

export interface LoaderProps {
    /**
     * css class name
     */
    className?: string;
    horizontal?: boolean;
    light?: boolean;
    title?: string;
}

const Loader = (props: LoaderProps) => {
    return (
        <div className={clsx("loader-container", props.horizontal && "loader-horizontal", props.className)}>
            <div className={clsx("loader", props.light && "loader-light")}></div>
            {props.title && <div className="loader-title">{props.title}</div>}
        </div>
    );
};

export default Loader;
