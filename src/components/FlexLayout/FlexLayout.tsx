import React from "react";
import clsx from "clsx";

type FlexProps = {
    children?: React.ReactNode;
    align?: "center" | "start" | "end";
    className?: string;
    direction?: "row" | "column";
    height?: string;
    justify?: "center" | "start" | "end" | "around" | "between";
    size?: "grow" | "shrink";
};

const Flex = React.forwardRef<HTMLDivElement, FlexProps>((props, ref) => {
    const componentProps: FlexProps = {
        className: clsx(
            "flex",
            props.align && "f-align-" + props.align,
            props.direction && "f-direction-" + props.direction,
            props.justify && "f-justify-" + props.justify,
            props.size && "f-size-" + props.size,
            props.className
        ),
        ...(props.height && {
            style: {
                height: props.height,
            },
        }),
    };

    return (
        <div {...componentProps} ref={ref}>
            {props.children}
        </div>
    );
});

export default Flex;
