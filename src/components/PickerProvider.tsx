// using this instead of the original from @material-ui/pickers
// applied locale and utils to this already

import React from "react";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import moment from "moment";
import MomentUtils from "@date-io/moment";

type PickerProviderProps = {
    children?: React.ReactNode;
    locale?: any;
};
const PickerProvider = ({ children, locale }: PickerProviderProps) => (
    <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={locale}>
        {children}
    </MuiPickersUtilsProvider>
);

export default PickerProvider;
