import React, { memo, useRef, useState } from "react";
import { Collapse, Input, InputProps } from "@material-ui/core";
import { useTranslation } from "react-i18next";

import { useUploadRowContext } from "./UploadRowContext";

import { debounce } from "../../helpers/utils";

type UploadRowCommentProps = Pick<InputProps, "onChange"> & {
    /**
     * default comment value
     */
    comment?: string;
};

const UploadRowComment = ({ comment, onChange }: UploadRowCommentProps) => {
    const { t } = useTranslation();
    const { cmtVisible } = useUploadRowContext();
    const [words, setWord] = useState(comment || "");
    const debounceRef = useRef(
        debounce(function (this: any) {
            onChange?.(this);
        }, 500)
    );

    const _onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        e.persist();
        if (e.target.value.length <= 1000) {
            setWord(e.target.value);
            const dispatchOnChange = debounceRef.current.bind(e);
            dispatchOnChange();
        }
    };

    return (
        <Collapse in={cmtVisible}>
            <div className={"upload-row-comment"}>
                <Input
                    placeholder={t("labels.comments")}
                    multiline
                    fullWidth
                    inputProps={{
                        tabIndex: cmtVisible ? 0 : -1,
                    }}
                    value={words}
                    onChange={_onChange}
                />
                <small className="upload-row-comment-wordcount">{`${words.length}`}/1000</small>
            </div>
        </Collapse>
    );
};

UploadRowComment.defaultProps = {
    comment: "",
};

export default memo(UploadRowComment, (prev, next) => true);
