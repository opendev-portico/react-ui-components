import React from "react";
import { useTranslation } from "react-i18next";

export type UploadRowHelperProps<
    E = {
        id?: string | null;
        error?: string | null;
        translate?: boolean | null;
    }
> = {
    errors?: E[];
    children?: React.ReactNode;
};

const UploadRowHelper = ({ children, errors }: UploadRowHelperProps) => {
    const { t } = useTranslation("common");
    return (
        <div className={"upload-row-helper"}>
            {errors ? (
                [errors]
                    .flat()
                    .filter(Boolean)
                    .map((error, i) => <span key={i}>{error.translate ? t(`error.${error.error}`) : error.error}</span>)
            ) : (
                <span>{children}</span>
            )}
        </div>
    );
};

export default UploadRowHelper;
