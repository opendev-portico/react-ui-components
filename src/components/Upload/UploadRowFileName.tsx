import React, { useCallback, useState } from "react";
import { TextField, TextFieldProps } from "@material-ui/core";

export type UploadRowFileNameProps = Pick<TextFieldProps, "placeholder"> & {
    children?: React.ReactNode;
    isReadOnly?: boolean;
    fileName?: string;
    onEditName?(name: string, ext: string): void;
};

const UploadRowFileName = ({ isReadOnly, fileName, placeholder, children, onEditName }: UploadRowFileNameProps) => {
    const n = fileName || "";
    const dot = n.lastIndexOf(".");
    const ext = n.slice(dot, n.length);
    const [name, setName] = useState(n.slice(0, dot));

    const _onChange = useCallback(
        (e: React.ChangeEvent<HTMLInputElement>) => {
            e.persist();
            const nm = e.target.value;
            setName(nm);
            if (nm !== n.slice(0, dot)) onEditName?.(nm, ext);
        },
        [n, onEditName, ext]
    );

    return isReadOnly ? (
        <div className="upload-row-filename">{children}</div>
    ) : (
        <TextField error={!name.length} value={name} placeholder={placeholder} onChange={_onChange} />
    );
};

UploadRowFileName.defaultProps = {
    fileName: "",
};

export default UploadRowFileName;
