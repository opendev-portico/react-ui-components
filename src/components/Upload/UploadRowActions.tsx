import React from "react";
import { FaComment, FaEdit, FaTimes } from "react-icons/fa";
import { IconButton } from "@material-ui/core";
import { Link as RouterLink } from "react-router-dom";

import { useUploadRowContext } from "./UploadRowContext";

import { addSearchParam } from "../../helpers/utils";
import { useRouter } from "../../hooks";

type UploadRowActionsProps = {
    /**
     *
     */
    relativeUrl?: string;
    /**
     * file data
     */
    fileData?: any;
    /**
     * flag to show/hide toggle comment button
     */
    commentable?: boolean;
    /**
     * remove file on list to upload
     */
    onDelete?(): void;
};

const UploadRowActions = ({ fileData, commentable, relativeUrl, onDelete }: UploadRowActionsProps) => {
    const { searchParams } = useRouter();
    const { setCmtVisible, cmtVisible, isReadOnly } = useUploadRowContext();

    const btnProps = isReadOnly
        ? {
              target: "_blank",
              component: RouterLink,
              to: {
                  search: addSearchParam(searchParams, "id", fileData.parentId).toString(),
                  pathname: `${relativeUrl ?? ""}/_forms/edit`,
              },
          }
        : {
              onClick: onDelete,
          };

    return (
        <div className="upload-row-actions">
            <IconButton tabIndex={-1} disableRipple title={isReadOnly ? "open edit form" : "remove file"} {...btnProps}>
                {isReadOnly ? <FaEdit /> : <FaTimes />}
            </IconButton>
            {commentable && !isReadOnly && (
                <IconButton tabIndex={-1} onClick={() => setCmtVisible(!cmtVisible)} title="file comment">
                    <FaComment />
                </IconButton>
            )}
        </div>
    );
};

UploadRowActions.defaultProps = {
    fileData: {},
};

export default UploadRowActions;
