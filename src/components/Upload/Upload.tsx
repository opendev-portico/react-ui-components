import React, { createContext, createElement, memo, useCallback, useContext, useEffect, useRef, useState } from "react";
import { Button, LinearProgress } from "@material-ui/core";
import { DropzoneOptions, DropzoneState, useDropzone } from "react-dropzone";
import { isValidElementType } from "react-is";
import { useTranslation } from "react-i18next";
import clsx from "clsx";
import isEqual from "fast-deep-equal/react";

import { LoadingLayout } from "../LoadingLayout";
import { Status } from "../LoadingLayout";
import FileRow, { UploadFileRowProps } from "./UploadFileRow";

import { isEmptyObject } from "../../helpers/dataChecker";

export type FileUploadProps = {
    file: File;
    comment?: string;
};

interface FileData {
    name: string;
    type: string;
    size: number;
    comment?: string;
}

type FileChangeEvent = "add" | "remove" | "changeName" | "editComment";

interface UploadChangeEvent {
    data: any;
    event?: FileChangeEvent;
    files: FileUploadProps[];
    valid?: boolean;
}

type UploadContextProps = {
    progress: number;
    speed: number;
    status: Status;
    files?: Record<string, any>;
    reset(): void;
};
export const UploadContext = createContext<UploadContextProps>({
    progress: 0,
    speed: 0,
    status: "ready",
    files: {},
    reset: () => {},
});

type UploadProps = DropzoneOptions & {
    children?(props: { dropzone: DropzoneState; UploadDisplay: React.ReactElement }): React.ReactElement;
    className?: string;
    editFile?: boolean;
    files?: FileUploadProps[];
    fullHeight?: boolean;
    onUploadChange?(event: UploadChangeEvent): void;
    Component?: {
        UploadFileRow?: React.ComponentType<UploadFileRowProps>;
    };
    ComponentProps?: {
        UploadFileRowProps?: Record<string, any>;
    };
    required?: boolean;
};

const Upload = ({
    onUploadChange,
    editFile,
    files,
    required,
    fullHeight,
    className,
    children,
    multiple,
    Component,
    ComponentProps,
    ...props
}: UploadProps) => {
    const uploadContext = useContext(UploadContext);
    if (process.env.mode === "development") {
        if (!uploadContext) {
            console.error("Please provide upload context to get Upload working properly");
            return null;
        }
    }
    const { progress, speed, status, reset } = uploadContext;
    const { t } = useTranslation("common");
    const prevFiles = useRef<FileUploadProps[]>(files ?? []);
    const validRef = useRef<Record<string, boolean>>({});

    const { UploadFileRow } = Component || { UploadFileRow: null };
    const { UploadFileRowProps } = ComponentProps || { UploadFileRowProps: null };

    const [fileData, setFileData] = useState<FileData[]>(
        prevFiles.current.map((f) => ({
            name: f.file.name,
            size: f.file.size,
            type: f.file.type,
            comment: f.comment,
        }))
    );
    const _updateFileData = useCallback(
        (fs: FileUploadProps[]) => {
            if (!files) {
                setFileData((_) =>
                    fs.map((f) => ({
                        name: f.file.name,
                        size: f.file.size,
                        type: f.file.type,
                        comment: f.comment,
                    }))
                );
            }
        },
        [files]
    );

    const _onUploadChange = useCallback(
        (event: FileChangeEvent, data: any) => {
            onUploadChange?.({
                event,
                data,
                files: prevFiles.current,
                valid: isEmptyObject(validRef.current),
            });
        },
        [onUploadChange]
    );

    const { ...dropzone } = useDropzone({
        accept: props.accept,
        noKeyboard: true,
        ...props,
        multiple,
        disabled: props.disabled,
        onDrop: (files) => {
            prevFiles.current = files.reduce((arr, file) => [...arr, { file }], multiple ? prevFiles.current : []);
            _updateFileData(prevFiles.current);
            _onUploadChange("add", files);
        },
    });

    const _changeFile = useCallback(
        (index: number) => (name: string, valid: boolean) => {
            validRef.current[index] = valid;
            if (valid) delete validRef.current[index];
            else validRef.current[index] = false;
            const f = prevFiles.current[index].file;
            prevFiles.current[index].file = new File([f], name, {
                type: f.type,
            });
            _onUploadChange("changeName", prevFiles.current[index]);
        },
        [_onUploadChange]
    );
    const _removeFile = useCallback(
        (index: number) => () => {
            delete validRef.current[index];
            prevFiles.current = prevFiles.current.filter((_, i) => i !== index);
            _updateFileData(prevFiles.current);
            _onUploadChange("remove", index);
        },
        [_onUploadChange]
    );
    const _onEditComment = useCallback(
        (index: number) => (_: any, comment: string) => {
            prevFiles.current[index].comment = comment;
            _updateFileData(prevFiles.current);
            _onUploadChange("editComment", comment);
        },
        [_onUploadChange]
    );

    useEffect(() => {
        if (files) {
            // set progress to 0 for repeat uploads
            reset();
            const fd = files.map((f) => ({
                name: f.file.name,
                size: f.file.size,
                type: f.file.type,
                comment: f.comment,
            }));
            if (!isEqual(fd, fileData)) {
                setFileData((_) => fd);
                prevFiles.current = files;
            }
        }
    }, [files]);

    return (
        <div
            className={clsx(
                "upload-container",
                {
                    fullHeight,
                    hasFile: prevFiles.current.length,
                },
                className
            )}>
            <LoadingLayout
                error={
                    <div className="error">
                        <div>{t("upload.fail")}</div>
                        <Button
                            onClick={() => {
                                reset();
                                //setStatus('ready');
                            }}>
                            {t("upload.retry")}
                        </Button>
                    </div>
                }
                status={status}
                loading={
                    <div className="upload-status">
                        <LinearProgress variant="determinate" value={progress} />
                        <div className="upload-status-stats">
                            <div className="upload-status-progress">{`${progress.toFixed(1)} %`}</div>
                            <div className="upload-status-speed">{`${speed.toFixed(1)} MB/s`}</div>
                        </div>
                    </div>
                }>
                <div
                    {...dropzone.getRootProps({
                        className: "dropzone",
                    })}>
                    <input {...dropzone.getInputProps()} />
                    {t("compound.dragToUpload")}
                </div>
            </LoadingLayout>
            {!!fileData.length && (
                <div className="filerows">
                    {fileData.map((file, index) =>
                        createElement(
                            isValidElementType(UploadFileRow) ? UploadFileRow : FileRow,
                            {
                                comment: file.comment,
                                editable: editFile,
                                fileName: file.name,
                                key: `${file.name}${index}`,
                                onEditFileName: _changeFile(index),
                                onDelete: _removeFile(index),
                                onEditComment: _onEditComment(index),
                                placeholder: t("upload.fileNamePlaceholder"),
                                size: file.size,
                                type: file.type,
                                ...UploadFileRowProps,
                            },
                            null
                        )
                    )}
                </div>
            )}
        </div>
    );
};

Upload.defaultProps = {
    multiple: false,
    ComponentProps: {},
    Component: {},
    files: [],
};

export default memo(Upload);
