import React, { memo, useContext, useMemo } from "react";

import { FileUploadProps, UploadContext } from "./Upload";
import { FlexLayout } from "../FlexLayout";
import UploadRow from "./UploadRow";
import UploadRowActions from "./UploadRowActions";
import UploadRowComment from "./UploadRowComment";
import UploadRowFileName from "./UploadRowFileName";
import UploadRowHelper from "./UploadRowHelper";

import { GetReadableFileSize } from "../../helpers/utils";

export type UploadFileRowProps = Pick<FileUploadProps, "comment"> & {
    /**
     * file of upload file
     */
    fileName: string;
    /**
     * enable/disable edit file name
     */
    editable?: boolean;
    /**
     * file name edit input placeholder
     */
    placeholder?: any;
    /**
     * file size
     */
    size: number;
    /**
     * file type
     */
    type: string;
    /**
     * flag to show/hide toggle comment button
     */
    commentable?: boolean;
    /**
     * change file name event
     * @param name: file name
     * @param valid: check file name validity, empty is invalid
     */
    onEditFileName: (name: string, valid: boolean) => void;
    /**
     * remove upload file on list
     */
    onDelete: () => void;

    /**
     * add comment to file event
     * @param comment comment text
     */
    onEditComment?(e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, comment: string): void;
};

const UploadFileRow = ({
    editable,
    placeholder,
    fileName,
    size,
    comment,
    commentable,
    type,
    onEditFileName,
    onDelete,
    onEditComment,
}: UploadFileRowProps) => {
    const uploadContext = useContext(UploadContext);
    if (process.env.mode === "development") {
        if (!uploadContext) {
            console.error("Please provide upload context to get Upload working properly");
            return null;
        }
    }
    const { files } = uploadContext;
    const fileData = useMemo(() => (files ? files[fileName] : {}), [files, fileName]);
    const uploaded = !!fileData;
    const error = uploaded && !!fileData.errors;

    const isReadOnly = !((editable && !uploaded) || error);

    return (
        <UploadRow isReadOnly={isReadOnly}>
            <FlexLayout size="grow" direction="column">
                <UploadRowFileName
                    fileName={fileName}
                    isReadOnly={isReadOnly}
                    placeholder={placeholder}
                    onEditName={(name, ext) => {
                        if (onEditFileName) onEditFileName(`${name}${ext}`, !!name);
                    }}>
                    {`${fileName} - ${GetReadableFileSize(size)}`}
                </UploadRowFileName>
                {commentable && (
                    <UploadRowComment comment={comment} onChange={(e) => onEditComment?.(e, e.target.value)} />
                )}
                <UploadRowHelper>
                    {!isReadOnly && <small>{`${fileName} - ${GetReadableFileSize(size)}`}</small>}
                </UploadRowHelper>
            </FlexLayout>
            <UploadRowActions fileData={fileData} commentable={commentable} onDelete={onDelete} />
        </UploadRow>
    );
};

export default memo(UploadFileRow);
