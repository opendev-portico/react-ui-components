import React, { useState } from "react";
import clsx from "clsx";

import UploadRowContext, { uploadRowContextDefault, UploadRowContextProps } from "./UploadRowContext";

import "./uploadRow.scss";

type UploadRowProps = Pick<UploadRowContextProps, "hasError" | "isReadOnly"> & {
    children?: React.ReactNode;
};

const UploadRow = ({ children, isReadOnly, hasError }: UploadRowProps) => {
    const [context, _setContext] = useState<UploadRowContextProps>(uploadRowContextDefault);
    return (
        <div
            className={clsx("upload-row", {
                "upload-row-comment-visible": context.cmtVisible,
                "upload-row-error": hasError,
            })}>
            <UploadRowContext.Provider value={{ ...context, isReadOnly, hasError, _setContext }}>
                {children}
            </UploadRowContext.Provider>
        </div>
    );
};

export default UploadRow;
