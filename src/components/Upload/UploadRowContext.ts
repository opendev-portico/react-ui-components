import { createContext, useContext } from "react";

export type UploadRowContextProps = {
    cmtVisible?: boolean;
    isReadOnly?: boolean;
    hasError?: boolean;
    _setContext?: React.Dispatch<React.SetStateAction<UploadRowContextProps>>;
};
export const uploadRowContextDefault: UploadRowContextProps = {
    cmtVisible: false,
    isReadOnly: false,
};
Object.freeze(uploadRowContextDefault);
const UploadRowContext = createContext<UploadRowContextProps>(uploadRowContextDefault);

export default UploadRowContext;

export const useUploadRowContext = () => {
    const { _setContext, ...context } = useContext(UploadRowContext);

    const setCmtVisible = (cmtVisible?: boolean) => {
        _setContext?.((prev) => ({ ...prev, cmtVisible }));
    };

    return {
        ...context,
        setCmtVisible,
    };
};
