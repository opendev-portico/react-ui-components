import React from "react";
import { Button } from "@material-ui/core";
import { FaExclamationCircle, FaMinusCircle, FaQuestionCircle } from "react-icons/fa";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import { FlexLayout } from "../FlexLayout";
import { GridLayout } from "../GridLayout";

type ErrorType = "noAccess" | "notFound";

interface ErrorLayoutProps {
    body?: React.ReactNode;
    errorType?: ErrorType;
    title?: React.ReactNode;
}

const errorIcon = (errorType?: ErrorType) => {
    switch (errorType) {
        case "noAccess":
            return <FaMinusCircle size={32} />;
        case "notFound":
            return <FaQuestionCircle size={32} />;
        default:
            return <FaExclamationCircle size={32} />;
    }
};

const ErrorLayout = (props: ErrorLayoutProps) => {
    const history = useHistory();
    const { t } = useTranslation("common");

    return (
        <FlexLayout align="center" justify="center" height="100%" size="grow" className="pad">
            <GridLayout className="text-center">
                <div>{errorIcon(props.errorType)}</div>
                <div>{props.body}</div>
                <div>
                    <Button onClick={() => history.goBack()}>{t("compound.goBack")}</Button>
                </div>
            </GridLayout>
        </FlexLayout>
    );
};

export default ErrorLayout;
