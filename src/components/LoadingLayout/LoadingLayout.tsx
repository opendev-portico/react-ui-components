import React from "react";
import { useTranslation } from "react-i18next";

import { ErrorLayout } from "../ErrorLayout";
import { Loader } from "../Loader";

export type Status = "fail" | "loading" | "ready" | "saving" | "success";

type LoadingLayoutProps = {
    children?: React.ReactNode;
    error?: React.ReactElement;
    errorText?: string;
    keepChildrenMounted?: boolean;
    loading?: React.ReactElement;
    loadingText?: string;
    saving?: React.ReactElement;
    savingText?: string;
    status?: Status;
};

const LoadingLayout = (props: LoadingLayoutProps) => {
    if (props.keepChildrenMounted)
        return (
            <>
                {["loading", "saving"].includes(props.status ?? "") && (
                    <div className="loading-overlay">
                        <LoadingLayoutRender {...props} />
                    </div>
                )}
                {props.children}
            </>
        );

    return <LoadingLayoutRender {...props} />;
};

LoadingLayout.defaultProps = {
    status: "ready",
};

const LoadingLayoutRender = (props: LoadingLayoutProps) => {
    const { t } = useTranslation("common");

    switch (props.status) {
        case "loading":
            return props.loading ?? <Loader title={props.loadingText ?? t("labels.loading")} />;
        case "saving":
            return props.saving ?? <Loader title={props.savingText ?? t("labels.saving")} />;
        case "fail":
            return props.error ?? <ErrorLayout body={props.errorText ?? t("labels.error")} />;
        default:
            if (!props.keepChildrenMounted) return <>{props.children}</>;
            return null;
    }
};

export default LoadingLayout;
