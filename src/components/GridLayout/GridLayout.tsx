import React, { CSSProperties } from "react";
import clsx from "clsx";

type GridProps = {
    children?: React.ReactNode;
    className?: string;
    cols?: number;
    responsive?: boolean;
    rows?: number;
    template?: Array<number>;
};

const Grid = React.forwardRef<HTMLDivElement, GridProps>((props, ref): React.ReactElement => {
    const componentProps: GridProps = {
        className: clsx(
            "grid",
            props.rows && `g-rows-${props.rows}`,
            props.cols && `g-cols-${props.cols}`,
            props.responsive && "g-responsive",
            props.className
        ),
        ...(props.template && {
            style: {
                gridTemplateColumns: props.template.join("fr ") + "fr",
            } as CSSProperties,
        }),
    };

    return (
        <div {...componentProps} ref={ref}>
            {props.children}
        </div>
    );
});

export default Grid;
