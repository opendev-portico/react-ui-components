import { useMemo } from "react";
import { useParams, useLocation, useHistory, useRouteMatch } from "react-router-dom";

export default function useRouter() {
    const params = useParams();
    const location = useLocation();
    const history = useHistory();
    const match = useRouteMatch();
    const searchParams = new URLSearchParams(location.search);
    // Return our custom router object
    // Memoize so that a new object is only returned if something changes
    return useMemo(() => {
        return {
            searchParams,
            action: history.action,
            // For convenience add push(), replace(), pathname at top level
            push: history.push,
            replace: history.replace,
            pathname: location.pathname,
            state: location.state as Record<string, string>,
            // Merge params and parsed query string into single "query" object
            // so that they can be used interchangeably.
            // Example: /:topic?sort=popular -> { topic: "react", sort: "popular" }

            query: {
                ..._iterateQueries(searchParams), // Convert string to object
                ...params,
            } as any,
            // Include match, location, history objects so we have
            // access to extra React Router functionality if needed.
            match,
            location,
            history,
        };
    }, [params, match, location, history]);
}

function _iterateQueries(params: URLSearchParams) {
    return Object.entries(params).reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {});
}
