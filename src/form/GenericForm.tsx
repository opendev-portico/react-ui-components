import React, { memo, useEffect, useState } from "react";
import { Formik, FormikHelpers, FormikConfig as FC } from "formik";

import { defaultSubmit } from "./helper";
import SubmitContext, { SetSubmitContext, SubmitContextProps } from "./SubmitContext";

type FormikConfig<D = any> = Omit<FC<D>, "isInitialValid" | "onSubmit">;

type SubmitEvent<V = any> = (
    values: V,
    formikHelpers: FormikHelpers<V>,
    setSubmitContext?: SetSubmitContext,
    resetSubmitContext?: () => void
) => Promise<any>;

type GenericFormProps<D = any> = Partial<FormikConfig<D>> & {
    formName?: string;
    submitContext?: Partial<SubmitContextProps>;
    onSubmit: SubmitEvent;
};

const GenericForm = ({ formName, onSubmit, submitContext, ...props }: GenericFormProps) => {
    const [context, _setContext] = useState<SubmitContextProps>({
        saveText: "labels.save",
        savingText: "labels.saving",
        savedText: "labels.saved",
        submitCount: 0,
        ...submitContext,
    });
    const setSubmitContext = (props: Partial<SubmitContextProps>) => {
        _setContext((prev) => {
            if (typeof props?.submitCount === "number") {
                props.submitCount = prev.submitCount + props?.submitCount;
            }
            return { ...prev, ...props };
        });
    };
    const resetSubmitContext = () => {
        _setContext({
            saveText: "labels.save",
            savingText: "labels.saving",
            savedText: "labels.saved",
            submitCount: 0,
        });
    };
    const _onSubmit: SubmitEvent = (values, helpers) => {
        return onSubmit ? onSubmit(values, helpers, setSubmitContext, resetSubmitContext) : defaultSubmit();
    };
    useEffect(() => {
        if (submitContext) {
            setSubmitContext(submitContext);
        }
    }, [submitContext]);
    return (
        <SubmitContext.Provider
            value={{
                ...context,
                setSubmitContext,
                resetSubmitContext,
            }}>
            <Formik onSubmit={_onSubmit} {...(props as any)} />
        </SubmitContext.Provider>
    );
};

GenericForm.defaultProps = {
    formName: "edit",
    enableReinitialize: false,
    validateOnBlur: false,
    validateOnMount: false,
    validateOnChange: false,
    initialValues: {},
};

export default memo(GenericForm);
