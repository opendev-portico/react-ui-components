import { GenericFieldHTMLAttributes, FieldProps } from "formik";

export type SharedInputProps<P = {}> = Omit<
    GenericFieldHTMLAttributes & {
        adornment?: React.ReactNode;
        defaultValue?: any;
        delay?: number;
        multiple?: boolean;
        name: string;
        value?: any;
        validateField?: string;
    },
    keyof P
> &
    P;

export type WithFormikProps<P = {}> = Omit<SharedInputProps<P>, keyof FieldProps> & FieldProps;
