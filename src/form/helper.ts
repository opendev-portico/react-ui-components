import { useEffect, useLayoutEffect, useRef } from "react";
import { FormikProps } from "formik";

export async function defaultSubmit() {
    console.error("Please provide submit handler");
    return await Promise.reject();
}

export const triggerValidate = (name: string, value: any, formik: FormikProps<any>, shouldValidate: boolean = true) => {
    const _mounted = useRef(false);
    const _timer = useRef<any>(null);
    useLayoutEffect(() => {
        if (!_mounted.current) return;
        if (shouldValidate) {
            clearTimeout(_timer.current);
            _timer.current = setTimeout(() => {
                formik.validateField(name);
            }, 200);
        }
    }, [value]);

    useEffect(() => {
        _mounted.current = true;
    }, []);
};

export const onChange = (
    name: string,
    value: any,
    formik: FormikProps<any>,
    // if validate right here, it will trigger validation in all fields, use at your own risk
    shouldValidate: boolean = false
) => {
    formik.setFieldValue(name, value, shouldValidate);
};
