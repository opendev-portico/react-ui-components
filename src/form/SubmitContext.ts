import React from "react";

export type SetSubmitContext = (context: Partial<SubmitContextProps>) => void;

export type SubmitContextProps = {
    saveText: string;
    savingText: string;
    savedText: string;
    submitCount: number;
    saveIcon?: JSX.Element;
    noSubmit?: boolean;
    noDisabled?: boolean;
    noIcon?: boolean;
    setSubmitContext?: SetSubmitContext;
    resetSubmitContext?(): void;
};

const SubmitContext = React.createContext<SubmitContextProps>({
    saveText: "",
    savingText: "",
    savedText: "",
    submitCount: 0,
});

export default SubmitContext;
