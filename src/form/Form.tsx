import React, { forwardRef, FormHTMLAttributes, RefAttributes } from "react";
import { Form as FormikForm, useFormikContext } from "formik";

type FormProps = FormHTMLAttributes<HTMLFormElement> &
    RefAttributes<HTMLFormElement> & {
        submitOnEnter?: boolean;
    };

const Form = forwardRef<HTMLFormElement, FormProps>(({ submitOnEnter, onKeyDown, ...props }, ref) => {
    const { submitForm } = useFormikContext();
    const _onKeyDown = (event: React.KeyboardEvent<HTMLFormElement>) => {
        if (submitOnEnter && event.key === "Enter") {
            submitForm();
        }
        onKeyDown?.(event);
    };
    return <FormikForm {...props} onKeyDown={_onKeyDown} ref={ref} />;
});

Form.defaultProps = {
    noValidate: true,
    submitOnEnter: false,
};

export default Form;
