import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import TextInput from "./TextInput";

export default {
    title: "Input/Text",
    component: TextInput,
    argTypes: {
        accessKey: { control: { type: "text" } },
        autoFocus: { control: { type: "boolean" } },
        classes: { control: { type: "text" } },
        className: { control: { type: "text" } },
        color: {
            options: ["primary", "secondary"],
            control: { type: "radio" },
        },
        defaultValue: { control: { type: "text" } },
        disabled: { control: { type: "boolean" } },
        error: { control: { type: "boolean" } },
        fullWidth: { control: { type: "boolean" } },
        helperText: { control: { type: "text" } },
        hidden: { control: { type: "boolean" } },
        hiddenLabel: { control: { type: "boolean" } },
        id: { control: { type: "text" } },
        innerRef: { control: null },
        label: { control: { type: "text" } },
        margin: { options: ["none", "dense", "normal"], control: { type: "radio" } },
        name: { control: { type: "text" } },
        onBlur: { control: null },
        onChange: { control: null },
        onClick: { control: null },
        onContextMenu: { control: null },
        onDoubleClick: { control: null },
        onFocus: { control: null },
        onKeyDown: { control: null },
        onKeyPress: { control: null },
        onKeyUp: { control: null },
        onTouchCancel: { control: null },
        onTouchEnd: { control: null },
        onTouchMove: { control: null },
        onTouchStart: { control: null },
        placeholder: { control: { type: "text" } },
        rows: { control: { type: "text" } },
        size: {
            options: ["small", "medium"],
            control: { type: "radio" },
        },
        tabIndex: { control: { type: "text" } },
        title: { control: { type: "text" } },
        value: { control: { type: "text" } },
        variant: { options: ["standard", "filled", "outlined"], control: { type: "radio" }, default: "outlined" },
        delay: { control: { type: "number" } },
        ref: { control: null },
        key: { control: null },
    },
} as ComponentMeta<typeof TextInput>;

export const Text: ComponentStory<typeof TextInput> = (args) => <TextInput {...args} />;
