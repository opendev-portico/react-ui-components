import React, { memo, useMemo } from "react";
import { FormControl, Switch } from "@material-ui/core";

import { WithFormikProps } from "../form/Input";

import { triggerValidate } from "../form/helper";

type CheckboxInputProps = WithFormikProps<{
    checked?: boolean;
}>;

const CheckboxInput = ({ field, meta, form, validateField, value, ...props }: CheckboxInputProps) => {
    const checked = useMemo(
        () =>
            Array.isArray(field.value)
                ? field.value.includes(value)
                : !!(value === undefined ? field.value : field.value === value),
        [field.value, value]
    );

    triggerValidate(validateField || field.name, field.value, form);

    return (
        <FormControl disabled={props.disabled} error={!!meta.error} required={props.required}>
            <Switch {...field} checked={checked} disabled={props.disabled} />
        </FormControl>
    );
};

export default memo(CheckboxInput);
