import React, { forwardRef, useEffect, useRef, useState } from "react";
import { TextField, TextFieldProps } from "@material-ui/core";

import { isNil } from "../helpers/dataChecker";

export type TextInputProps = Pick<
    TextFieldProps,
    | "accessKey"
    | "autoFocus"
    | "classes"
    | "className"
    | "color"
    | "defaultValue"
    | "disabled"
    | "error"
    | "FormHelperTextProps"
    | "fullWidth"
    | "helperText"
    | "hidden"
    | "hiddenLabel"
    | "id"
    | "innerRef"
    | "InputLabelProps"
    | "inputProps"
    | "InputProps"
    | "label"
    | "margin"
    | "name"
    | "onBlur"
    | "onChange"
    | "onClick"
    | "onContextMenu"
    | "onDoubleClick"
    | "onFocus"
    | "onKeyDown"
    | "onKeyPress"
    | "onKeyUp"
    | "onTouchCancel"
    | "onTouchEnd"
    | "onTouchMove"
    | "onTouchStart"
    | "placeholder"
    | "rows"
    | "size"
    | "style"
    | "tabIndex"
    | "title"
    | "value"
    | "variant"
> & {
    /**
     * delay onChange event trigger
     */
    delay?: number;
};

const TextInput = forwardRef<HTMLDivElement, TextInputProps>(({ delay, onChange, ...props }, ref) => {
    const [value, setValue] = useState(props.value ?? props.defaultValue);
    const timer = useRef<any>(null);

    const _onChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
        e.persist();
        setValue(e.target.value);
        clearTimeout(timer.current);

        timer.current = setTimeout(() => {
            onChange?.(e);
        }, Number(delay) ?? 0);
    };

    useEffect(() => {
        if (!isNil(props.value) && value !== props.value) {
            setValue(props.value);
        }
    }, [props.value]);

    useEffect(() => {
        return () => {
            clearTimeout(timer.current);
        };
    }, []);

    return <TextField {...props} value={value} onChange={_onChange} ref={ref} />;
});

export default TextInput;
