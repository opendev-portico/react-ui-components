import React, { memo } from "react";

import { WithFormikProps } from "../form/Input";
import { onChange, triggerValidate } from "../form/helper";

import TinyEditor from "../components/TinyEditor";

type HTMLInputProps = WithFormikProps<{}>;

const HTMLInput = ({ field, form, meta, validateField, ...props }: HTMLInputProps) => {
    const onEditorChange = (content: any, _editor: any) => {
        onChange(field.name, content, form);
    };

    triggerValidate(validateField || field.name, field.value, form);

    return (
        <TinyEditor
            disabled={props.disabled}
            init={{
                forced_root_block: false,
                contextmenu: false,
                menubar: false,
                quickbars_insert_toolbar: "quicktable image media codesample",
                quickbars_selection_toolbar: false,
                //quickbars_selection_toolbar: 'bold italic underline | formatselect | blockquote quicklink',
                toolbar: "bold italic underline | link unlink",
                height: 120,
                inline: true,
                plugins: ["link"],
            }}
            value={field.value}
            onEditorChange={onEditorChange}
        />
    );
};

export default memo(HTMLInput);
