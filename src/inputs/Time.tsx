import React, { memo } from "react";
import { FaClock } from "react-icons/fa";
import { InputAdornment } from "@material-ui/core";
import { TimePicker } from "@material-ui/pickers";

import { WithFormikProps } from "../form/Input";
import { onChange, triggerValidate } from "../form/helper";

import MuiPickersUtilsProvider from "../components/PickerProvider";

type TimeInputProps = WithFormikProps<{}>;

const TimeInput = ({ field, form, meta, validateField, ...props }: TimeInputProps) => {
    const _onChange = (date: any) => {
        onChange(field.name, date ? date.toDate() : null, form);
    };

    triggerValidate(validateField || field.name, field.value, form);

    return (
        <MuiPickersUtilsProvider>
            <TimePicker
                {...field}
                value={field.value}
                autoOk={true}
                clearable={true}
                disabled={props.disabled}
                InputProps={{
                    startAdornment: (
                        <InputAdornment position="start">
                            <FaClock />
                        </InputAdornment>
                    ),
                }}
                onChange={_onChange}
            />
        </MuiPickersUtilsProvider>
    );
};

export default memo(TimeInput);
