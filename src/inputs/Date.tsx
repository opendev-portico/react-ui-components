import React, { memo } from "react";
import { KeyboardDateTimePicker } from "@material-ui/pickers";

import { WithFormikProps } from "../form/Input";
import { onChange, triggerValidate } from "../form/helper";

import MuiPickersUtilsProvider from "../components/PickerProvider";

type DateInputProps = WithFormikProps<{}>;

const DateInput = ({ field, form, meta, validateField, ...props }: DateInputProps) => {
    const _onChange = (date: any) => {
        onChange(field.name, date ? date.toDate() : null, form);
    };

    triggerValidate(validateField || field.name, field.value, form);

    return (
        <MuiPickersUtilsProvider>
            <KeyboardDateTimePicker
                {...field}
                autoOk={true}
                clearable={true}
                disabled={props.disabled}
                error={!!meta.error}
                format="DD-MM-YYYY hh:mm a"
                InputAdornmentProps={{
                    position: "start",
                }}
                KeyboardButtonProps={{
                    tabIndex: -1,
                }}
                className={"date-picker"}
                onChange={_onChange}
                placeholder={props.placeholder}
                variant="dialog"
                value={field.value || null}
                clearLabel={"Clear"}
                okLabel={"OK"}
                cancelLabel={"Cancel"}
                todayLabel={"Today"}
            />
        </MuiPickersUtilsProvider>
    );
};

export default memo(DateInput);
