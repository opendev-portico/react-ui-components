export const isUndefined = (value: any) => {
    return value === undefined;
};
export const isNull = (value: any) => {
    return value === null;
};
export const isNil = (value: any) => {
    return isNull(value) || isUndefined(value);
};
export const isArray = (value: any) => {
    return Array.isArray(value);
};
export const isEmptyArray = (value: any) => {
    return isArray(value) ? !value.length : true;
};
export const isObject = (value: any) => {
    return !!(value && typeof value === "object" && (value as Object).constructor === Object);
};
export const isEmptyObject = (value: any) => {
    return isObject(value) ? !Object.keys(value).length : true;
};
export const isObjectLiked = (value: any) => {
    return isArray(value) || isObject(value);
};
export const isEmpty = (value: any) => {
    return isNil(value) || value === "" || isEmptyObject(value) || isEmptyArray(value);
};
export const isDate = (value: any) => {
    return value instanceof Date;
};
export const isSymbol = (value: any) => {
    return typeof value === "symbol";
};
export const isError = (value: any) => {
    return value instanceof Error && typeof value.message !== "undefined";
};
export const isRegExp = (value: any) => {
    return !!(value && typeof value === "object" && (value as Object).constructor === RegExp);
};
export const isFunction = (value: any) => {
    return typeof value === "function";
};
export const isFolderData = (data: any) => {
    return isObject(data) && data.hasOwnProperty("ParentFolderId");
};
export const getEnumList = <T extends {}>(en: T) => {
    return Object.entries(en).filter(([_, value]) => typeof value === "number");
};
interface WebEditorSupport {
    supported: boolean;
    editorSlug?: "_spreadsheet" | "_document" | "_pdf";
}

export const checkEditorSupportsFile = (fileName: string): WebEditorSupport => {
    if (fileName != null && fileName.length > 1) {
        const re: RegExp = new RegExp(/(?:\.([^.]+))?$/);
        const ext = re.exec(fileName)?.[0];
        switch (ext?.toLowerCase()) {
            case ".dotx":
            case ".docx":
            case ".docm":
            case ".dotm":
            case ".dot":
            case ".doc":
            case ".rtf":
            case ".txt":
            case ".xml":
                return {
                    supported: true,
                    editorSlug: "_document",
                };
            case ".xls":
            case ".xlsx":
            case ".csv":
                return {
                    supported: true,
                    editorSlug: "_spreadsheet",
                };
            case ".pdf":
                return {
                    supported: true,
                    editorSlug: "_pdf",
                };
            default:
                return {
                    supported: false,
                };
        }
    }
    return {
        supported: false,
    };
};
