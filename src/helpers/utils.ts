export function addSearchParam(search: URLSearchParams | string, key: string, value: any) {
    const result = new URLSearchParams(search);
    if (result.has(key)) {
        result.set(key, value);
    } else {
        result.append(key, value);
    }
    return result;
}

export const debounce = (handler: any, delay = 0, now = false) => {
    let timeout: NodeJS.Timeout | null;
    return function (this: any) {
        const context = this,
            args = arguments;
        const later = function () {
            timeout = null;
            if (!now) handler.apply(context, args);
        };
        const callNow = now && !timeout;
        if (timeout !== null) clearTimeout(timeout);
        timeout = setTimeout(later, delay);
        if (callNow) handler.apply(context, args);
    };
};

export const GetReadableFileSize = (fileSizeInBytes: number) => {
    var i = -1;
    var byteUnits = [" kB", " MB", " GB", " TB", "PB", "EB", "ZB", "YB"];
    do {
        fileSizeInBytes = fileSizeInBytes / 1024;
        i++;
    } while (fileSizeInBytes > 1024);

    return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
};
