module.exports = {
    root: true,
    env: {
        amd: true,
        browser: true,
        commonjs: true,
        es6: true,
        jest: true,
        node: true,
        worker: true,
        "jest/globals": true,
    },
    settings: {
        react: {
            version: "detect",
        },
        "import/parsers": {
            "@typescript-eslint/parser": [".ts", ".tsx"],
        },
        "import/resolver": {
            typescript: {},
        },
        jest: {
            version: 26,
        },
    },
    extends: [
        "prettier/@typescript-eslint",
        "eslint:recommended",
        "plugin:@typescript-eslint/eslint-recommended",
        "plugin:@typescript-eslint/recommended",
        "plugin:import/errors",
        "plugin:import/warnings",
        "plugin:jest/recommended",
        "plugin:jsx-a11y/recommended",
        "plugin:react/recommended",
        "plugin:prettier/recommended",
    ],
    parserOptions: {
        sourceType: "module",
        ecmaFeatures: {
            jsx: true,
        },
    },
    parser: "@typescript-eslint/parser",
    plugins: ["jest", "jsx-a11y", "react", "react-hooks", "@typescript-eslint", "import"],
    rules: {
        "@typescript-eslint/no-unused-vars": ["warn", { argsIgnorePattern: "^_" }],
        "@typescript-eslint/no-explicit-any": "off",
        "jsx-a11y/no-onchange": "off", // https://github.com/evcohen/eslint-plugin-jsx-a11y/issues/398
        "jsx-quotes": ["error", "prefer-single"],
        "no-trailing-spaces": "error",
        "object-curly-spacing": ["error", "always"],
        quotes: ["error", "single", { allowTemplateLiterals: true }],
        "react-hooks/exhaustive-deps": "error",
        "react-hooks/rules-of-hooks": "error",
        "react/prop-types": "off",
        semi: ["error", "never"],
        "import/no-named-as-default": "off",
        "import/no-named-as-default-member": "off",
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "jest/no-disabled-tests": "warn",
        "jest/no-focused-tests": "error",
        "jest/no-identical-title": "error",
        "jest/prefer-to-have-length": "warn",
        "jest/valid-expect": "error",
    },
};
