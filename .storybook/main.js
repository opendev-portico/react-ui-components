module.exports = {
    stories: ["../src/**/*.stories.mdx", "../src/**/*.stories.@(js|jsx|ts|tsx)"],
    addons: [
        "@storybook/addon-links",
        "@storybook/addon-essentials",
        "@storybook/preset-scss",
        {
            name: "@storybook/addon-postcss",
            options: {
                postcssLoaderOptions: {
                    implementation: require("postcss"),
                },
            },
        },
    ],
    webpackFinal: async (config, { configType }) => {
        // `configType` has a value of 'DEVELOPMENT' or 'PRODUCTION'
        // You can change the configuration based on that.
        // 'PRODUCTION' is used when building the static version of storybook.

        // // find the DefinePlugin
        // const plugin = config.plugins.find((plugin) => plugin.definitions?.["process.env"]);
        // // add my env vars
        // Object.keys(appConfig).forEach((key) => {
        //     plugin.definitions["process.env"][key] = appConfig[key];
        // });

        return config;
    },
    typescript: {
        // check: false,
        // checkOptions: {},
        reactDocgen: "react-docgen-typescript",
        reactDocgenTypescriptOptions: {
            // shouldExtractLiteralValuesFromEnum: true,
            // propFilter: (prop) => {
            //     return prop.parent
            //         ? /@material-ui/.test(prop.parent.fileName) || !/node_modules/.test(prop.parent.fileName)
            //         : true;
            // },
        },
    },
};
