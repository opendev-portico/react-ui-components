module.exports = {
  endOfLine: "auto",
  jsxBracketSameLine: false,
  jsxBracketSameLine: true,
  printWidth: 120,
  tabWidth: 4,
  trailingComma: "es5",
  useTabs: false,
};
