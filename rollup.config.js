import pkg from "./package.json";

// import copy from "rollup-plugin-copy";
// import json from "@rollup/plugin-json";
// import sourcemaps from 'rollup-plugin-sourcemaps'
import { nodeResolve } from "@rollup/plugin-node-resolve";
import { terser } from "rollup-plugin-terser";
import commonjs from "@rollup/plugin-commonjs";
import dts from "rollup-plugin-dts";
import image from "@rollup/plugin-image";
import peerDepsExternal from "rollup-plugin-peer-deps-external";
import postcss from "rollup-plugin-postcss";
import typescript from "@rollup/plugin-typescript";

const extensions = [".js", ".jsx", ".es6", ".es", ".mjs", ".ts", ".tsx", ".scss", ".css"];
const resolveConfig = { extensions };

export default [
    /**
     * CommonJS (for Node) and ES module (for bundlers) build.
     * (We could have three entries in the configuration array
     * instead of two, but it's quicker to generate multiple
     * builds from a single configuration where possible, using
     * an array for the `output` option, where we can specify
     * `file` and `format` for each target)
     */
    {
        input: ["./src/index.ts"],
        output: [
            { file: pkg.main, format: "cjs", plugins: [terser()] },
            { file: pkg.module, format: "es", plugins: [terser()] },
            { file: "dist/index.js", format: "cjs", sourcemap: true },
            { file: "dist/index.mjs.js", format: "es", sourcemap: true },
        ],
        plugins: [
            peerDepsExternal(),
            image(),
            typescript({}),
            commonjs(),
            // sourcemaps(),
            nodeResolve(resolveConfig),
            postcss({
                // extract: true,
                // extract: 'index.css',
            }),
            // copy({
            //     targets: [
            //         { src: "src/index.d.ts", dest: "dist" },
            //         { src: "global.d.ts", dest: "dist" },
            //     ],
            // }),
        ],
    },
    {
        input: "./dist/dts/index.d.ts",
        output: [{ file: "dist/index.d.ts", format: "es" }],
        external: [/\.scss$/],
        plugins: [dts()],
    },
];
