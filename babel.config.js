module.exports = function (api) {
    api.cache(true);

    const presets = [
        [
            "@babel/preset-env",
            {
                modules: false,
                useBuiltIns: "usage",
                corejs: {
                    version: 3.8,
                    proposals: true,
                },
            },
        ],
        "@babel/preset-typescript",
        "@babel/preset-react",
    ];
    const plugins = [
        [
            "@babel/plugin-transform-runtime",
            {
                // absoluteRuntime: false,
                corejs: {
                    version: 3,
                    proposals: true,
                },
                // helpers: true,
                // regenerator: true,
                useESModules: true,
                // version: '7.0.0-beta.0',
            },
        ],
    ];

    return {
        presets,
        plugins,
    };
};
